var fs = require("fs");
//Converts csv data to object

let path1 = "./matches.csv";
let path2 = "./deliveries.csv";

let getData = pat => {
  return new Promise((resolve, reject) => {
    fs.readFile(pat, "utf-8", (err, info) => {
      if (err) {
        reject(err);
      } else {
        var file = info;
        // fs.readFileSync(path1, {
        //   encoding: "utf-8"
        // });

        file = file.split("\n");
        //console.log(file);
        let heading = file.shift().split(",");
        //console.log(heading);
        heading.splice(heading.length - 1, 1); //to remove umpire3
        //console.log(heading);
        let data = [];

        file.forEach(function(d) {
          let tmp = {};
          let row = d.split(",");
          //console.log(heading[0]);
          for (let i = 0; i < heading.length; i++) {
            tmp[heading[i]] = row[i];
          }
          data.push(tmp);
          //console.log(data);
        });
        resolve(data);
      }
    });
  });
};
let matchData;
let deliverieData;
let getData1 = () => {
  return new Promise((resolve, reject) => {
    getData(path1).then(abc => {
      matchData = abc;
    });
    getData(path2).then(abc => {
      deliverieData = abc;
    });
    resolve(matchData, deliverieData);
  });
};
let getData3 = (matchData, deliverieData) => {
  let id = [];
  for (let i = 0; i < matchData.length; i++) {
    if (matchData[i]["season"] === "2016") {
      id.push(matchData[i]["id"]);
    }
  }
  let objExtra = {};
  for (let i = 0; i < id.length; i++) {
    for (let j = 0; j < deliverieData.length; j++) {
      if (deliverieData[j]["match_id"] === id[i]) {
        if (deliverieData[j]["bowling_team"] in objExtra) {
          objExtra[deliverieData[j]["bowling_team"]] += parseFloat(
            deliverieData[j]["extra_runs"]
          );
        } else {
          objExtra[deliverieData[j]["bowling_team"]] = parseFloat(
            deliverieData[j]["extra_runs"]
          );
        }
      }
    }
  }
  console.log(objExtra);
};
let finalFunctin = () => {
  getData1().then((a, b) => {
    getData3(a, b);
  });
};

//9999999999999999999999999999999999999999999999999999999999999999999999999
// function dataSplit(path) {
//   var file = fs.readFileSync(path, {
//     encoding: "utf-8"
//   });

//   file = file.split("\n");
//   //console.log(file);
//   let heading = file.shift().split(",");
//   //console.log(heading);
//   heading.splice(heading.length - 1, 1); //to remove umpire3
//   //console.log(heading);
//   let data = [];

//   file.forEach(function(d) {
//     let tmp = {};
//     let row = d.split(",");
//     //console.log(heading[0]);
//     for (let i = 0; i < heading.length; i++) {
//       tmp[heading[i]] = row[i];
//     }
//     data.push(tmp);
//     //console.log(data);
//   });
//   return data;
// }

//let matchData = dataSplit("./matches.csv");
//let deliverieData = dataSplit("./deliveries.csv");

//console.log(matchData);
//console.log(deliverieData);

// export const getNoOfMatchesPlayed = () => {
//   let yearArr = [];
//   let noMatches = [];
//   let year;
//   let index;
//   let rv = {};

//   for (let i = 0; i < data.length; i++) {
//     year = data[i].season;
//     if (yearArr.includes(year)) {
//       index = yearArr.indexOf(year);
//       noMatches[index] += 1;
//     } else {
//       yearArr.push(year);
//       noMatches.push(1);
//     }
//   }
//   // console.log(yearArr);
//   // console.log(noMatches);
//   // console.log(yearArr.length)
//   for (let i = 0; i < yearArr.length; i++) {
//     //obj[toString(yearArr[i])] = noMatches[i];
//     rv[yearArr[i].toString()] = noMatches[i];
//   }
//   //console.log(rv);
//   return rv;
// };
// export const getNoOfMatchesWonPerTeamPerYear = () => {
//   let teamWinPerYear = {};
//   let year;
//   let winTeaem;
//   for (let i = 0; i < data.length; i++) {
//     year = data[i].season;
//     winTeaem = data[i].winner;
//     if (year.toString() in teamWinPerYear) {
//       if (winTeaem in teamWinPerYear[year.toString()]) {
//         teamWinPerYear[year.toString()][winTeaem] += 1;
//       } else {
//         teamWinPerYear[year.toString()][winTeaem] = 1;
//       }
//     } else {
//       teamWinPerYear[year.toString()] = {};
//       teamWinPerYear[year][winTeaem] = 1;
//     }
//   }
//   return teamWinPerYear;
// };
// export const getExtraRunsPerTeamForYear = () => {
//   let id = [];

//   for (let i = 0; i < matchData.length; i++) {
//     if (matchData[i]["season"] === "2016") {
//       id.push(matchData[i]["id"]);
//     }
//   }
//   let objExtra = {};
//   for (let i = 0; i < id.length; i++) {
//     for (let j = 0; j < deliverieData.length; j++) {
//       if (deliverieData[j]["match_id"] === id[i]) {
//         if (deliverieData[j]["bowling_team"] in objExtra) {
//           objExtra[deliverieData[j]["bowling_team"]] += parseFloat(
//             deliverieData[j]["extra_runs"]
//           );
//         } else {
//           objExtra[deliverieData[j]["bowling_team"]] = parseFloat(
//             deliverieData[j]["extra_runs"]
//           );
//         }
//       }
//     }
//   }
//   return objExtra;
// };
// export const getEconomicalBowlersForYear = () => {
//   let match_id_2015 = new Set();
//   let extra_runs = {};
//   for (let i = 0; i < matchData.length; i++) {
//     if (matchData[i].season === "2015") {
//       match_id_2015.add(matchData[i].id);
//     }
//   }
//   match_id_2015 = Array.from(match_id_2015);
//   let players = new Set();
//   for (let i = 0; i < deliverieData.length; i++) {
//     if (match_id_2015.includes(deliverieData[i].match_id)) {
//       players.add(deliverieData[i].bowler);
//     }
//   }
//   players = Array.from(players);
//   let tot_runs_player = {};
//   for (let i = 0; i < players.length; i++) {
//     tot_runs_player[players[i]] = 0;
//   }

//   let runs = 0;
//   let overs = 0;
//   let prev = 0;
//   let eco_rates = new Array();
//   for (let i = 0; i < players.length; i++) {
//     for (let j = 0; j < deliverieData.length; j++) {
//       if (match_id_2015.includes(deliverieData[j].match_id)) {
//         if (deliverieData[j].bowler === players[i]) {
//           runs += parseInt(deliverieData[j].total_runs);
//           if (prev !== deliverieData[j].over) {
//             overs += 1;
//             prev = deliverieData[j].over;
//           }
//         }
//       }
//     }
//     tot_runs_player[players[i]] = runs / overs;
//     eco_rates.push([players[i], runs / overs]);
//     runs = 0;
//     overs = 0;
//     prev = 0;
//   }
//   eco_rates.sort(function(a, b) {
//     return a[1] - b[1];
//   });
//   eco_rates.splice(10, eco_rates.length - 1);
//   return eco_rates;
// };

// //
