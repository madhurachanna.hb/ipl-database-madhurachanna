function abc(){
  var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () {
  if (this.readyState == 4 && this.status == 200) {
    // Typical action to be performed when the document is ready:
    //document.getElementById("demo").innerHTML = xhttp.responseText;
    //console.log(xhttp.responseText);
    let dataData = JSON.parse(xhttp.responseText)
    //temp = JSON.parse(xhttp.responseText);
    //console.log(temp);
    Highcharts.chart('container1', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Number of matches played per year for all the years in IPL.'
      },
      subtitle: {
        text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
      },
      xAxis: {
        type: "category"
      },
      yAxis: {
        title: {
          text: 'Number of matches played '
        }

      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: '{point.y}'
          }
        }
      },

      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
      },
      "series": dataData,
    });
  }
};
xhttp.open("GET", "sol1.json", true);
xhttp.send();







}

abc();

function def() {
  var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () {
  if (this.readyState == 4 && this.status == 200) {
    // Typical action to be performed when the document is ready:
    //document.getElementById("demo").innerHTML = xhttp.responseText;
    //console.log(xhttp.responseText);
    let dataData2 = JSON.parse(xhttp.responseText)
    //temp = JSON.parse(xhttp.responseText);
    //console.log(temp);

    Highcharts.chart('container2', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Number of matches won of per team per year in IPL.'
      },
      subtitle: {
      },
      xAxis: {
        categories: [
            "Chennai Super Kings",
            "Rajasthan Royals",
            "Royal Challengers Bangalore",
            "Mumbai Indians",
            "Pune Warriors",
            "Kolkata Knight Riders",
            "Kings XI Punjab",
            "Deccan Chargers",
            "Kochi Tuskers Kerala",
            "Delhi Daredevils",
            "abc"
        ],
        title: {
          text: null
        }
      },
      yAxis: {
        // min: 0,
        // title: {
        //   text: 'Population (millions)',
        //   align: 'high'
        // },
        // labels: {
        //   overflow: 'justify'
        // }
      },
      tooltip: {
        valueSuffix: ' millions'
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: dataData2,
    });
  }
};
xhttp.open("GET", "sol2.json", true);
xhttp.send();
}

def();




function ghi(){
  var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () {
  if (this.readyState == 4 && this.status == 200) {
    // Typical action to be performed when the document is ready:
    //document.getElementById("demo").innerHTML = xhttp.responseText;
    //console.log(xhttp.responseText);
    let dataData = JSON.parse(xhttp.responseText)
    //temp = JSON.parse(xhttp.responseText);
    //console.log(temp);
    Highcharts.chart('container3', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Extra runs conceded per team in 2016'
      },
      subtitle: {
        text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
      },
      xAxis: {
        type: "category",
      },
      yAxis: {
        title: {
          text: 'Number of matches played '
        }

      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: '{point.y}'
          }
        }
      },

      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
      },
      "series": dataData,
    });
  }
};
xhttp.open("GET", "sol3.json", true);
xhttp.send();
}

ghi();




function jkl(){
  var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () {
  if (this.readyState == 4 && this.status == 200) {
    // Typical action to be performed when the document is ready:
    //document.getElementById("demo").innerHTML = xhttp.responseText;
    //console.log(xhttp.responseText);
    let dataData = JSON.parse(xhttp.responseText)
    //temp = JSON.parse(xhttp.responseText);
    //console.log(temp);
    Highcharts.chart('container4', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Top 10 economical bowlers in 2015'
      },
      subtitle: {
        text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
      },
      xAxis: {
        type: "category"
      },
      yAxis: {
        title: {
          text: 'Number of matches played '
        }

      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: '{point.y}'
          }
        }
      },

      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
      },
      "series": dataData,
    });
  }
};
xhttp.open("GET", "sol4.json", true);
xhttp.send();
}

jkl();